// Create Font
PFont fontA;

void setup() 
{
  size(1000, 500);
  noLoop();
  
  //*****************
  // Load a text file with separate values (just an integer number) on each line
  // the text file is converted to an integer array which can be input to the decision tree generator
  
  int[] output_tennis = loadData("output_tennis.txt");
  
  int[] outlook = loadData("outlook.txt");
  
  int[] temp = loadData("temp.txt");
 
  int[] humidity = loadData("humidity.txt");
  
  int[] wind = loadData("wind.txt");
  
  //******************
  // INPUT Information Tennis
  
  // OUTPUT Information
  String[] output_labels_tennis = {"No", "Yes"};
  
  // INPUT #0 Outlook
  int num_outlook_levels = 3;   // 0 = overcast, 1 = rain, 2 = sunny
  String[] outlook_labels = {"Overcast", "Rain", "Sunny"};
  
  // INPUT #1 Temp
  int num_temp_levels = 3;      // 0 = cool, 1 = mild, 2 = hot
  String[] temp_labels = {"Cool", "Mild", "Hot"};
  
  // INPUT #3 Humidity
  int num_humidity_levels = 2;  // 0 = normal, 1 = high
  String[] humidity_labels = {"Normal", "High"};
  
  // INPUT #4 Wind
  int num_wind_levels = 2;     // 0 = weak, 1 = strong
  String[] wind_labels = {"Weak", "Strong"};
  
  // INPUT Information
  int[][] input_attributes_tennis = {outlook, temp, humidity, wind};
  String[] attribute_labels_tennis = {"Outlook", "Temp", "Humidity", "Wind"};
  String[][] attribute_level_labels_tennis = {outlook_labels, temp_labels, humidity_labels, wind_labels};
  Decision_Tree tree_tennis;
  int leave_out_tennis = 2;
  
  //*****************
  int[] node = {1, 1};
  float window_w = width;
  tree_tennis = new Decision_Tree("play_tennis", output_tennis, output_labels_tennis, input_attributes_tennis, attribute_labels_tennis, attribute_level_labels_tennis, leave_out_tennis, node, window_w);
}

void draw() {
  
}

//*************************************************
//*************************************************
//*************************************************
//        DECISION TREE CLASS DEFINITION
//*************************************************
//*************************************************
//*************************************************
class Decision_Tree
{
  // PRIVATE OBJECT VARIABLES //
  private int[] _outs;                        // An array of output classifications, like [yes, no, yes...] or [left, right, center, right...]
                                              //   each classification must be an integer value 0, 1, 2 ... n, where n is the number of output classes
  private String[] _output_labels;            // An array of output classification labels, like ["yes", "no"...] or ["left", "right", "center"...]
                                              //   each label must be a String that corresponds to one of the output classifications integers.
                                              //   This means if there are 3 output classifications then there should also be 3 output lable Strings
  private int _num_output_levels;             // Number of different output classifications
  private int[][] _attributes;                // An array of arrays (matrix) of attribute classifications. Each column represents the values of a given attribute such 
                                              //   as [Temperature, Humidity, Wind...] and the column array for a given attribute contains he values of that
                                              //   attribute, like [hot, cold, cool, cool, hot, cold...]. Each value for a given attribute must be an integer
                                              //   value 0, 1, 2 ... m, where m is the number of levels the particular attribute can take on. Each attribute may
                                              //   take on a unique number of discrete levels.
  private int[] _num_attribute_levels;        // An array which contains the number of attribute levels for each attribute. The order of these attributes must
                                              //   match that in the _attributes matrix
  private int _num_attributes;                // Number of different attributes
  private String[] _attribute_labels;         // An array which contains labels for each attribute, like ["Temperature", "Humidity", "Wind"...]
                                              //   these labels must be Strings and must match the order of the attributes in the _attributes matrix
  private String[][] _attribute_level_labels; // An array of arrays (matrix) of attribute level labels. Each column represents a different attribute. The labels
                                              //   within a column are the labels that correspond to the different attribute levels for a given attribute. If a 
                                              //   given attribute has m levels it can take on, then it must also have m labels for those attributes. Each attribute
                                              //   may have a unique number of attributes corresponding to its unique number of attribute levels
  private int _num_data_points;               // Number of differnt ordered groupings of intputs (attributes) and outputs. All attributes and output must have this
                                              //   number of values in their array of values adn they must all be in corresponding order for the decision tree to
                                              //   produce meaningful results
  private float[] _attribute_gains;           // An array which contains the information gain value for each attribute in the tree, these may be any real number
  private int _max_attribute_index;           // Number which corresponds to the row index of the attribute which provides the maximum information gain
  private int _num_max_attribute_levels;      // Number of attribute levels (levels the attribute can take on) for whichever attribute provides maximum information gain
  private float _collection_entropy;          // The entropy of the outputs for the current tree. If all output are the same, then entropy = 0. If split evenly entropy = 1
                                              //   This value may be any real number between 0 and 1
  private int[] _node;
  private float _w;
  private String _fileName;
  
  // CONSTRUCTOR //
  Decision_Tree(String fileName, int[] outputs, String[] output_labels, int[][] attributes, String[] attribute_labels, String[][] attribute_level_labels, int leave_out, int[] b_node, float w) 
  { 
    // In the constructor we set our object's private variables with the data passed to it
    
    // Set the number of output levels
    _num_output_levels = output_labels.length;
    // Set the number of data points to the number of output minus the number of data points we don't want build the tree with
    _num_data_points = outputs.length - leave_out;
    // Set the number of different attributes
    _num_attributes = attributes.length;
    // Create an array to hold the attribute gains, make it large enough to old a gain value for each attribute
    _attribute_gains = new float[_num_attributes];
    // Set the outs based on the passed array of outputs
    _outs = outputs;
    // Set the number of attribute levels
    int[] temp_num_a_levels = new int[_num_attributes];
    for (int i = 0; i < _num_attributes; i++) {
      temp_num_a_levels[i] = attribute_level_labels[i].length;
    }
    _num_attribute_levels = temp_num_a_levels;
    // Set the matrix with the passed matrix of attributes
    _attributes = attributes;
    // Set the output labels
    _output_labels = output_labels;
    // Set the attribute name labels
    _attribute_labels = attribute_labels;
    // Set the attribute level labels matrix
    _attribute_level_labels = attribute_level_labels;
    // Set the starting node for this tree (a node indicates the branch and level)
    _node = b_node;
    // Set the width of the graphical output for this tree
    _w = w;
    _fileName = fileName;
    
    // Initialize some of the graphical properties of the tree including strok and fill weight and color
    strokeJoin(ROUND);
    strokeWeight(1);
    stroke(142, 83, 44);
    fill(142, 83, 44);
    
    // If this is the origin node of the tree (indicated by [1, 1]) then do some initialization steps
    if(_node[0] == 1 && _node[1] == 1) {
      background(255);
      // Load the font for the graphical output and set its properties
      fontA = loadFont("CourierNewPSMT-32.vlw");
      textFont(fontA, 16);
      textAlign(CENTER);
      // Print the title for the tree in the graphical output
      text(_fileName+" Decision Tree", _w/2, 25);
      textFont(fontA, 14);
      translate(0, 50);
      // Place the origin node ellipse in the graphical output
      ellipse(_w/2, 0, 5, 5);
      
      // Create the File We want to use and add to 
      createTreeFile(_fileName);
    }
    
// **** PRINT DEBUG STATEMENT **** //    
    print("Node = [" + _node[0] + " " + _node[1] + "] ");
    
    // Calculate the entropy present in the output using the private function calculate_collection_entropy()
    _collection_entropy = calculate_collection_entropy();
    // If the collection entropy returns 0, then the leaf is determined, there is only one option
    if(_collection_entropy == 0) {
// **** PRINT DEBUG STATEMENT **** //
      println("Collection Entropy = 0, Leaf = " + _output_labels[outputs[0]] + " " + outputs[0]);
      
      // Place a "Leaf" with the final decision in the graphical output
      drawLeaf(_output_labels[outputs[0]]);
      // Add leaf to autogenerated code file
      addLeaf(outputs[0]);
      // If we place a leaf node, then the branch is terminated, so return from the decision tree constructor
      return;
    }
    // If the collection entropy returns -1, then there are no example inputs for this branch, so we'll simply
    //   choose a random leaf decision and place it on the end of the branch
    else if(_collection_entropy == -1.0) {
      // Since we have no information about what to do with this branch, simply choose a uniform random number
      // from 0 to the number of output levels we have and set this as the leaf node.
      int random_leaf = int(random(_num_output_levels));
// **** PRINT DEBUG STATEMENT **** //
      println("Collection Entropy = -1, Random Leaf = " + _output_labels[random_leaf] + " " + random_leaf);
      
      // Place a "Leaf" with the final decision in the graphical output
      drawLeaf(_output_labels[random_leaf]);
      // Add leaf to autogenerated code file
      // Add the random_choices array of possibilies to the generated function
      PrintWriter output = appendWriter(_fileName + ".h");
      output.print(tabOver(_node[0])+"return random("+_num_output_levels+");\t");
      output.println("// " + _fileName + " = random choice");
      output.close();
      
      // If we place a leaf node, then the branch is terminated, so return from the decision tree constructor
      return;
    }

// **** PRINT DEBUG STATEMENT **** //
    print("Collection Entropy = ");
    print(_collection_entropy);
    print(", ");
    
    // If we get this far then it means we do not yet have an entropy of 0 (determined decision) yet, so we need
    // to check if we can keep going down this path. We can unless we have already branched on as many attributes
    // as we have available. If _num_attributes equals zero, it means we know know more branches to make, so we 
    // need to choose a leaf node to terminate the branch. We'll use a weighted random selection which chooses the
    // leaf randomly based on the relative frequency of the output classifications for this particular branch
    if (_num_attributes == 0) {
      // Because we know we don't have zero entropy and we know we are out of attributes to check we must make a
      // weighted random choice of the leaf node to terminate the branch
      int wrandom_leaf = choose_leaf();
// **** PRINT DEBUG STATEMENT **** //
      println("No More Attributes to Check, Weighted Random Leaf " + _output_labels[wrandom_leaf] + " " + wrandom_leaf);
      
      // Place a "Leaf" with the final decision in the graphical output
      // and add leaf to autogenerated code file (with weighted random code generated as well)
      drawLeaf(_output_labels[wrandom_leaf]);
      // If we place a leaf node, then the branch is terminated, so return from the decision tree constructor
      return;
    }
    
    // For each attribute that we have, measure its information gain to determine which attribute to branch on
    for(int attribute_index = 0; attribute_index < _num_attributes; attribute_index++) {
      _attribute_gains[attribute_index] = calculate_attribute_gains(attribute_index);
    }
    // After we have calculated the information gain for each attribute pick the maximum attribute gain
    float max_gain = max(_attribute_gains);
    
// **** PRINT DEBUG STATEMENT **** //
    print("\nMax Attribute Gain = ");
    print(max_gain);
    print(", ");
    
    // For each attribute that we have compare its information gain to the maximum information gain so we can
    // determine which attribute provides the maximum attribute gain. Save the index number for this attribute
    // in _max_attribute_index
    for(int attribute_index = 0; attribute_index < _num_attributes; attribute_index++) {
      if (_attribute_gains[attribute_index] == max_gain) {
        _max_attribute_index = attribute_index;
      }
    }
    
// **** PRINT DEBUG STATEMENT **** /
    print("Maximum Gain Attribute = ");
    println(_attribute_labels[_max_attribute_index]);
    
    // Set _num_max_attribute_levels to be equal to the number of levels the attribute that provides maximum
    // attribute gains can take on.
    _num_max_attribute_levels = _num_attribute_levels[_max_attribute_index];
    
    // For each attribute level of the attribute that provides the maximum information gain create a branch
    // Each branch is effectively another smaller decision tree. Within the branch_on_max_attribute() method
    // The decision tree constructor is recursively called, this is how the entire tree is built from the root node
    for(int alevel_index = 0; alevel_index < _num_max_attribute_levels; alevel_index++) {
      _node[1] = alevel_index + 1;
// **** PRINT DEBUG STATEMENT **** /
      print("Attribute Branch = ");
      print(_attribute_level_labels[_max_attribute_index][alevel_index]);
      print(" ");
      
      // Print Branch information to autogenerated code file
      PrintWriter output = appendWriter(_fileName + ".h");
      output.print(tabOver(_node[0]));
      if(alevel_index != 0){
        output.print("else ");
      }
      output.print("if("+_attribute_labels[_max_attribute_index]+" == "+alevel_index+"){");
      output.println("\t// " + _attribute_labels[_max_attribute_index]+" == "+_attribute_level_labels[_max_attribute_index][alevel_index]+"?");
      output.close();
      
      // pushMatrix saves the current graphical output coordinate system to the stack
      pushMatrix();
      // The amount of x axis translation is an integer sum the current graphical output width divided by (the number of branches on this level + 1)
      float x_translation = _w/(_num_max_attribute_levels+1);
      // Set the strok weight color and fill color
      strokeWeight(2);
      stroke(142, 83, 44);
      fill(142, 83, 44);
      // Create a line from the previous node to the current node
      line(_w/2, (_node[0]-1)*50, x_translation *(_node[1]), _node[0]*50);
      strokeWeight(1);
      // Translate to the position of the current node
      translate(x_translation *(_node[1]), 0);
      fill(51, 108, 48);
      // Place a node ellipse and label at the location of the current node
      text(_attribute_level_labels[_max_attribute_index][alevel_index], 0, _node[0]*50 - 10);
      fill(142, 83, 44);
      ellipse(0, _node[0]*50, 5, 5);
      // Translate to the center of the current nodes's graphical width before branching down to next level
      translate(-x_translation/2, 0);
      // Create a branch for each level of the maximum attribute
      branch_on_max_attribute(alevel_index, _max_attribute_index);
      // After returning from the recursive branch, pop the coordinate matrix back off the stack
      popMatrix();
      
      // Print Terminating "}" for each braching "if/else" statement
      output = appendWriter(_fileName + ".h");
      output.print(tabOver(_node[0]));
      output.println("}");
      output.close();
    }
    
    // After finishing all the sub branches place the branch question label
    fill(0);
    text(_attribute_labels[_max_attribute_index]+"?", _w/2, (_node[0]-1)*50 + 15);
    
    if(_node[0] == 1) {
      // Finish and close the autogenerated decision tree file
      closeTreeFile(_fileName);
      validationOutput(outputs.length, leave_out);
    }
  }
  
  // PRIVATE METHODS //
  
  // Method Name: drawLeaf()
  // Inputs: String (s, the label of the final decision output);
  // Outputs: Void
  // Description: Use this method to place a graphical "leaf" in the graphical output. The "leaf" is simply
  //              the label of the final output decision for that terminal node
  private void drawLeaf(String s) {
      // Set the stroke and fill color
      stroke(51, 108, 48);
      fill(51, 108, 48);
      // push the current coordinate system matrix to the stack before doing system manipulations
      pushMatrix();
      // Translate and roate the coorinate system to make leaf labels run vertically out of their nodes
      translate(_w/2, (_node[0]-1)*50+10);
      rotate(PI/2);
      textAlign(LEFT);
      text(s, 1, 3);
      textAlign(CENTER);
      // pop the coordinate system matrix back off the stack in order to return to the previous coordinate system
      popMatrix();
  }
  
  // Method Name: choose_leaf()
  // Inputs: Void
  // Outputs: Int (weighted random leaf node)
  // Description: Use this method to terminate a decision branch with non-zero entropy which still contains members
  //              The leaf node selected is random but weigted in favor of outputs with a greater frequency of examples
  //              This function also adds the random number generator code to the generated Arduino function!
  private int choose_leaf(){
    // An array of integers, tallies, contains the number of examples for a given output level
    int[] tallies = new int[_num_output_levels];
    // Integer that represents the number of total outputs of all levels on this branch
    int tally_sum = 0;
    
    // For each data point compare its output level to each of the avaiable output levels
    // and increment the tally corresponding to the output level it matched
    for (int data_index = 0; data_index < _num_data_points; data_index++) {
      for (int output_level = 0; output_level < _num_output_levels; output_level++) {
        if (_outs[data_index] == output_level) {
          tallies[output_level]++;
          tally_sum++;
        }
      }
    }
    
    // Create an integer array with size equal to the total number of outputs on the branch    
    int[] random_choices = new int[tally_sum];
    // Integer that represents the index of the random_choices array
    int choice_index = 0;
    // For each output level, fill as many slots in the random_choices array with that given output level
    // as there were tallies calculated for that given output level. This is how the outputs with more examples
    // are more heavily weighted in the random selection, simply because they fill more slots in the random selection array.
    for (int output_level = 0; output_level < _num_output_levels; output_level++) {
      for(int n = 0; n < tallies[output_level]; n++) {
        // Set the value of the current random_choices slot with the current output_level
        random_choices[choice_index] = output_level;
        // Increment the choice_index so the whole random_choices array can be filled
        choice_index++;
      }
    }
    // Randomly choose an integer from 0 to the number of examples on the branch, and use this as the index to look
    // in the random_choices array. By using the value in that index location as the random leaf node, we have effectively
    // created a random selection weighted in favor of examples which are more frequent.
    int random_leaf = random_choices[int(random(tally_sum))];
    
    // We add this same code to the Arduino Function, so that as the Arduino executes the decision tree its choices for
    // undetermined states vary randomly and are not stuck in the random configuration generated at time of code generation
    // Open the file for writing
    PrintWriter output = appendWriter(_fileName + ".h");
    
    // Add the random_choices array of possibilies to the generated function
    output.print(tabOver(_node[0])+"int random_choices[] = {");
    for (int i = 0; i < random_choices.length; i++) {
      output.print(random_choices[i]);
      if(i != random_choices.length-1){
        output.print(", ");
      }
    }
    output.println("};");
    output.print(tabOver(_node[0])+"return random_choices[random("+tally_sum+")];\t");
    output.println("// " + _fileName + " = weighted random choice");
    output.close();
    
    // Return the randomly selected terminal leaf node output value (an integer)
    return random_leaf;
  }
  
  // Method Name: branch_on_max_attribute()
  // Inputs: int (attribute_level, the level of the maximum information gain attribute we wish to branch on)
  //         int (max_attribute_index, the index corresponding to the attribute which provides maximum information gain)
  // Outputs: Void
  // Description: Use this method to effectively create a smaller decision tree branched around the selected attribute level and attribute
  //              This method recursively calls the decision tree constructor, thereby allowing the object to build itself from the root node
  private void branch_on_max_attribute(int attribute_level, int max_attribute_index) {
    // The target_attribute is the column of the _attributes matrix which corresponds to the max_attribute_index
    int[] target_attribute = _attributes[max_attribute_index];
    // We must keep track of the number of datapoints of the current attribute actually have the attribute level we are branching on
    int num_data_points_with_attribute = 0;
    // For each data point of the target_attribute array that has a value equal to the target attribute level, we
    // increment the num_data_points_with_attribute index.
    for(int data_index = 0; data_index < _num_data_points; data_index++) {
      if(target_attribute[data_index] == attribute_level) {
        num_data_points_with_attribute++;
      }
    }
    
    // Now we will create new output, attribute and label arrays by stripping the global arrays and matrixes with those values down to the 
    // data points corresponding to the maximum gain attribute and its target level
   
    // We will have an array of outputs with the same number of memembers as num_data_points_with_attribute 
    int[] outputs = new int[num_data_points_with_attribute];
    
    // We will have a temporary matrix of attributes with the number of columns as the global number of attributes minus one, with rows for
    // each of the global data points
    int[][] temp_attributes = new int[_num_attributes - 1][_num_data_points];
    
    // The final new attribute matrix with the number of columns as the global number of attributes minus one, with a number of rows equal
    // to num_data_points_with_attribute
    int[][] attributes = new int[_num_attributes - 1][num_data_points_with_attribute];
    
    // The new attribute_labels array will have labels for all the global attributes, except the one we are branching on
    String[] attribute_labels = new String[_num_attributes - 1];
    
    // The new attribute_level_labels matrix will have columns for each global attribute, except the one we are branching on,
    // and it will have a number of rows which varies from attribute to attribute, based on the number of levels each one has
    String[][] attribute_level_labels = new String[_num_attributes - 1][];
    
    // Since we are now inside the decision tree, we no longer want to leave out any data points in building the rest of the tree
    int leave_out = 0;
    
    // The integer, new_at_index, keeps track of the index to use in the new arrays 
    int new_at_index = 0;
    
    // For each of the global attributes update the new arrays and matrixes
    for(int at_index = 0; at_index < _num_attributes; at_index++) {
        // As long as the attribute index does not equal the attribute we are branching on, update the arrays and matrixes to match it
        if(at_index != max_attribute_index) {
          temp_attributes[new_at_index] = _attributes[at_index];
          attribute_labels[new_at_index] = _attribute_labels[at_index];
          attribute_level_labels[new_at_index] = _attribute_level_labels[at_index];
          new_at_index++;
        }
    }
    
    // The integer, new_data_index, keeps track of the index to use in the new data arrays
    int new_data_index = 0;
    // For each data point with a level for our target attribute that matches the target level, we want to save it
    // Effectively we are creating smaller arrays with the output and attribute datapoints that are selected based on the target level
    // of the maximum gain attribute. These data arrays will be fed to the next level of the decision tree as inputs
    for(int data_index = 0; data_index < _num_data_points; data_index++) {
      // If the current data_index of the target_attribute matches our target attribute level then
      if(target_attribute[data_index] == attribute_level) {
        // Save the output for that data index
        outputs[new_data_index] = _outs[data_index];
        // And also, for each attribute save that data index as well
        for(int at_index = 0; at_index < _num_attributes - 1; at_index++) {
          attributes[at_index][new_data_index] = temp_attributes[at_index][data_index];
        }
        // Increment the new_data_index to keep track of where we are in the new data arrays  
        new_data_index++;
      }
    }
    
    // Create a new "node" to pass to the recursive tree branch
    int[] pass_node = new int[2];
    pass_node[0] = _node[0] + 1;
    pass_node[1] = _node[1];
    
    float _width = _w/(_num_max_attribute_levels+1);
    // Now that we have created all our outputs, levels, labels, and attributes, levels and lables we can create a new decision tree, which is essentially now just a sub-branch
    // of the larger overall tree. This is where the recursion comes in and makes the decision tree possible.
    Decision_Tree branch = new Decision_Tree(_fileName, outputs, _output_labels, attributes, attribute_labels, attribute_level_labels, leave_out, pass_node, _width);  
}
  
  // Method Name: calculate_attribute_gains()
  // Inputs: int (attribute_num, the index corresponding to the attribute who's information gain we want to know.)
  // Outputs: float (the information gain of the chosen attribute)
  // Description: Use this method to determine the information gain for a given attribute in the current branch/tree
  private float calculate_attribute_gains(int attribute_num) {
    // Integer number of attribute levels of the chosen attribute
    int num_a_levels = _num_attribute_levels[attribute_num];
    // Integer matrix with a tally for the number of each attribute level that have a given output level associated with them
    int[][] tallies = new int[num_a_levels][_num_output_levels];
    // Integer array which keep track of the number data points at each given attribute level, regardless of output level
    int[] a_sum = new int[num_a_levels];
    // Float matrix with a number for the ratio of tally/a_sum fore each attribute level and outputlevel combination
    float[][] peas = new float[num_a_levels][_num_output_levels];
    // Float matrix with a number for the log2 partial entropy for each value in peas[][]
    float[][] logs = new float[num_a_levels][_num_output_levels];
    // Float array with an entropy value for each attribute level
    float entropies[] = new float[num_a_levels];
    
    // For each data point continue
    for (int data_index = 0; data_index < _num_data_points; data_index++) {
      // For each attribute level if the current attribute datapoint matches the current attribute level continue 
      for (int alevel_index = 0; alevel_index < num_a_levels; alevel_index++) {
        if (_attributes[attribute_num][data_index] == alevel_index) {
          // For each output level compare if the current output datapoint matches the output level
          // increment the corresponding tally and a_sum
          for (int olevel = 0; olevel < _num_output_levels; olevel++) {
            if (_outs[data_index] == olevel) {
              tallies[alevel_index][olevel]++;
              a_sum[alevel_index]++;
            }
          }
        }
      }
    }
    
    // For each attribute level and each output level with that attribute level
    for (int alevel_index = 0; alevel_index < num_a_levels; alevel_index++) {
      for (int olevel = 0; olevel < _num_output_levels; olevel++) {
        // Calculate the value of peas as a float ratio of the tally with that attribute level and output level
        // divided by the number with that attribute level
        peas[alevel_index][olevel] = (float)tallies[alevel_index][olevel]/a_sum[alevel_index];
        // If the value of peas is zero or NaN (meaning divide by zero error), then there were no tallies with this attribute level and output
        // and we define 0*log2(0) = 0.
        if(peas[alevel_index][olevel] == 0 || a_sum[alevel_index] == 0) {
          logs[alevel_index][olevel] = 0;
        }
        // Else, the tallies were not zero so we actually need to calculate the log partial entropy value
        // Recall log2(x) = log(x)/log(2)
        else {
          logs[alevel_index][olevel] = -peas[alevel_index][olevel]*(log(peas[alevel_index][olevel]))/(log(2));
        }
        // The total entropy for a given attribute level is the sum of all the log partial entropy values for that attribute level 
        entropies[alevel_index] = entropies[alevel_index] + logs[alevel_index][olevel];
      }
    }
    
    // The float gain will be the information gain of the chosen attribute
    float gain = 0;
    // Information gain is calculated as the collection entropy minus the sum of all the entropies for each attribute level multiplied with their
    // respective ratio of total data points with that attribute level and the total number of data points.
    for (int alevel_index = 0; alevel_index < num_a_levels; alevel_index++) {
      gain = gain + (a_sum[alevel_index]/((float)_num_data_points))*entropies[alevel_index];
    }
    gain = _collection_entropy - gain;
    
    // Return the float information gain of the chosen attribute
    return gain;
  }
  
  // Method Name: calculate_collection_entropy()
  // Inputs: Void
  // Outputs: float (the entropy of the entire collection, the entropy of the outputs)
  // Description: Use this method to determine the entropy of the entire collection, the output entropy, which is necessary to calculate
  //              information gain.
  private float calculate_collection_entropy() {
    // Integer array to hold the number of data points whith each output level
    int[] tallies = new int[_num_output_levels];
    // Float array to hold the ratio of tallies to total datapoints
    float[] peas = new float[_num_output_levels];
    // Float array to hold the partial log2 entropy values for each value in peas
    float[] logs = new float[_num_output_levels];
    // Float entropy will be the returned value of collection entropy, initialized to zero
    float entropy = 0;
    // Tally to keep track of the total number of datapoints
    int tally_sum = 0;
    
    // For each possible output level at each data point check if the actual output level matches
    // If it does increment the appropriate tally to keep track of the number at that output level and total number of outputs
    for (int data_index = 0; data_index < _num_data_points; data_index++) {
      for (int output_level = 0; output_level < _num_output_levels; output_level++) {
        if (_outs[data_index] == output_level) {
          tallies[output_level]++;
          tally_sum++;
        }
      }
    }
    
    // If the there were datapoints with matching outputs, then return an entropy of -1.0, which means an error occured,
    // and the output data field is empty
    if(tally_sum == 0) {
      return -1.0;
    }
    
    // For each output_level calculate peas as the ratio of the number of datapoints with that output level
    // divided by the total number of data points
    for (int output_level = 0; output_level < _num_output_levels; output_level++) {
      peas[output_level] = tallies[output_level]/(float)_num_data_points;
      // If the value of peas is zero or NaN (meaning divide by zero error), then there were no tallies with this attribute level and output
      // and we define 0*log2(0) = 0.
      if (peas[output_level] == 0) {
        logs[output_level] = 0;
      }
      // Else, the tallies were not zero so we actually need to calculate the log partial entropy value
      // Recall log2(x) = log(x)/log(2)
      else {
        logs[output_level] = -peas[output_level]*((log(peas[output_level]))/log(2.0));
      }
      // The total entropy the sum of all the log partial entropy values for all the output levels 
      entropy += logs[output_level];
    }
    // Return the total entropy, the collection output entropy
    return entropy; 
  }
  
  // Method Name: createTreeFile()
  // Inputs: String (out_f_name, file name for auto-generated code file);
  // Outputs: void
  // Description: Use this method to initially create and generate the auto-generated code file
  //              This method generates the file, header comments, include statements and the tree
  //              function definition
  private void createTreeFile(String out_f_name){
    // Create a PrintWriter object associated with the file we are generating
    PrintWriter out_f = createWriter(out_f_name + ".h");
    // Begin printing initial banner comments including:
    // Month, date and time of code generation
    out_f.println("/* " + out_f_name + " decision Tree file for Arduino\t*/");
    String[] monthName = {"","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
    out_f.println("/* Date: " +  day() + " " + monthName[month()] +  " " +  year() + "\t\t\t\t\t*/" );
    out_f.println("/* Time: " +  hour() + ":" + minute() + ":" + second()+"\t\t\t\t\t\t*/"); 
    out_f.println();
    
    // Add the proper #ifndef and #define statements to allow the generated code to function
    // with the Arduino system
    out_f.println("#ifndef " + out_f_name + "_H");
    out_f.println("#define " + out_f_name + "_H\n");
    
    out_f.println("#if defined(ARDUINO) && ARDUINO >= 100");
    out_f.println("#include \"Arduino.h\"");
    out_f.println("#else");
    out_f.println("#include \"WProgram.h\"");
    out_f.println("#endif\n");
    
    // Add a list of all the output classes and their numerical value representations
    out_f.println("// \tOutput (Decision):");
    out_f.print("// "+out_f_name+"\tlevels: ");
    for (int i = 0; i < _num_output_levels; i++){
      out_f.print(i+" = "+_output_labels[i]+" ");
    }
    out_f.println();
    
    // Add a list of all the attributes, their levels and the numerical representations for each
    out_f.println("// \tInput (Attributes):");
    for (int i = 0; i < _num_attributes; i++) {
      out_f.print("// "+ _attribute_labels[i]+"\tlevels: ");
      for(int j = 0; j < _num_attribute_levels[i]; j++) {
        out_f.print(j+" = "+_attribute_level_labels[i][j]+" ");
      }
      out_f.println();
    }
    out_f.println();
    
    // Add the function definition for the tree function itself
    // The function always returns type int and has an interger input for each attribute,
    // as well as an input which will be the seed value for the random number generator
    out_f.print("\nint " + out_f_name + "(");
    for (int i = 0; i < _num_attributes; i++) {
      out_f.print("int " + _attribute_labels[i]+", ");
    }
    out_f.println("int random_seed){");
    out_f.println("\trandomSeed(random_seed); // seed the psuedo random number generator");
    
    // After writing all data flush the PrintWriter and close it    
    out_f.flush(); // Write the remaining data
    out_f.close();
  }
  
  // Method Name: closeTreeFile()
  // Inputs: String (filename, the filename of the autogenerated code file);
  // Outputs: void
  // Description: Use this method to end the autogenerated code file with the proper
  // "}" and "#endif" statements
  private void closeTreeFile(String filename){
    PrintWriter out_f = appendWriter(filename + ".h");
    out_f.println("}");
    out_f.println("\n#endif");
    out_f.flush(); // Write the remaining data
    out_f.close(); // Finish the file
  }
  
  // Method Name: appendWriter()
  // Inputs: String (filename, the filename of the file we wish to append);
  // Outputs: PrintWriter (a PrintWriter object associated with the file we wish to append)
  // Description: Use this method to get a PrintWriter object wich can be used to append to
  //              and existing file. If the file name specified does not already exist, then
  //              an exception occurs
  private PrintWriter appendWriter(String filename) {
    try {
	File file = saveFile(filename);
	OutputStream output = new FileOutputStream(file, true);
	return createWriter(output);
    } catch (Exception e) {
	e.printStackTrace();
	throw new RuntimeException("Couldn't create a writer for " + filename);
    }
  }
 
  // Method Name: tabOver()
  // Inputs: int (n, number of tabs to add);
  // Outputs: String (a String which simply consists of the number of tab characters requested)
  // Description: Use this method to build a string of multiple tab characters. This method is
  //              used to align sections of the the autogenerated code
  private String tabOver(int n){
    String tabs = "\t";
      for (int i = 0; i < n-1; i++) {
        tabs = tabs + "\t";
      }
      return tabs;
  }
  
  // Method Name: addLeaf()
  // Inputs: int (leafNum, the number of the leaf's class and label);
  // Outputs: Void
  // Description: Use this method to add a leaf decision to the auto-generated arduino code
  private void addLeaf(int leafNum){
    PrintWriter output = appendWriter(_fileName + ".h");
    output.print(tabOver(_node[0])+"return "+leafNum+";\t");
    output.println("// " + _fileName + " = " +_output_labels[leafNum]);
    output.close();
  }
  
  private void validationOutput(int outputLength, int leaveOut){
    println("\n\n VALIDATION OUTPUT \n");
    if(leaveOut == 0) {
      return;
    } else {
      print("int " + _fileName +"[] = {");
      for(int i = 0; i < leaveOut; i++){
        print(_outs[outputLength - leaveOut + i]);
        if(i != leaveOut-1) {
          print(", ");
        }
      }
      println("};");
      
      for(int atNum = 0; atNum < _num_attributes; atNum++) {
        print("int "+ _attribute_labels[atNum]+"[] = {");
        for(int i = 0; i < leaveOut; i++){
          print(_attributes[atNum][_attributes[atNum].length - leaveOut + i]);
          if(i != leaveOut-1) {
            print(", ");
          }
        }
        println("};");
      }
    }
  }
}

// Method Name: convertStringArraytoIntArray()
// Inputs: String[] (sarray, the String array to be converted to an int array);
// Outputs: int[] (the new array of ints converted from the array of Strings)
// Description: Use this method to convert an array of Strings to an array of integers
//              This method assumes each String consists of a single number with no
//              punctuation
public int[] convertStringArraytoIntArray(String[] sarray) {
  // If the array of Strings is not empty or NULL then process it
  if (sarray != null) {
    int intarray[] = new int[sarray.length];              // Create int array with elements for each String in String array
    for (int i = 0; i < sarray.length; i++) {
      intarray[i] = Integer.parseInt(sarray[i]);          // For each String in String array, use parseInt to get integer
    }
    return intarray;                                      // Return the interger array
  }
  return null;                                            // If the String array was null, return null
}


public int[] loadData(String filename) {
  String[] dataStrings = loadStrings(filename);
  return convertStringArraytoIntArray(dataStrings);
}
