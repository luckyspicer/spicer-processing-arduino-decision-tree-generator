/* play_tennis decision Tree file for Arduino	*/
/* Date: 7 Mar 2012					*/
/* Time: 11:29:22						*/

#ifndef play_tennis_H
#define play_tennis_H

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

// 	Output (Decision):
// play_tennis	levels: 0 = No 1 = Yes 
// 	Input (Attributes):
// Outlook	levels: 0 = Overcast 1 = Rain 2 = Sunny 
// Temp	levels: 0 = Cool 1 = Mild 2 = Hot 
// Humidity	levels: 0 = Normal 1 = High 
// Wind	levels: 0 = Weak 1 = Strong 


int play_tennis(int Outlook, int Temp, int Humidity, int Wind, int random_seed){
	randomSeed(random_seed); // seed the psuedo random number generator
	if(Outlook == 0){	// Outlook == Overcast?
		return 1;	// play_tennis = Yes
	}
	else if(Outlook == 1){	// Outlook == Rain?
		if(Wind == 0){	// Wind == Weak?
			return 1;	// play_tennis = Yes
		}
		else if(Wind == 1){	// Wind == Strong?
			return 0;	// play_tennis = No
		}
	}
	else if(Outlook == 2){	// Outlook == Sunny?
		if(Humidity == 0){	// Humidity == Normal?
			return 1;	// play_tennis = Yes
		}
		else if(Humidity == 1){	// Humidity == High?
			return 0;	// play_tennis = No
		}
	}
}

#endif
